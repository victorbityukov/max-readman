import React, {Component} from 'react';
import './App.css';
import {connect, Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Contents from "./components/Contents/Contents";
import {checkMe} from "./redux/auth-reducer";
import {getSettings} from "./redux/settings-reducer";

class App extends Component {
  componentDidMount() {
    this.props.checkMe();
    this.props.getSettings();
  }

  render() {
    return (
      <div className="App">
        <Provider store={this.props.store}>
          <BrowserRouter>
            <Header/>
            <Contents/>
            <Footer/>
          </BrowserRouter>
        </Provider>
      </div>
    );
  }
}


let mapStateToProps = () => {
  return {}
};

let mapDispatchToProps = (dispatch) => {
  return {
    checkMe: () => {
      dispatch(checkMe());
    },
    getSettings: () => {
      dispatch(getSettings());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
