import {settingsAPI} from "../api/api";
const SET_SETTINGS = 'SET_SETTINGS';
const SET_BLACK_SECTION = 'SET_BLACK_SECTION';

let initialState = {
  config: {},
  isReady: false,
  isReadyBlack: false,
  blackSection: {}
};

const settingsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SETTINGS:
      return {
        ...state,
        config: action.data,
        isReady: true
      };
    case SET_BLACK_SECTION:
      return {
        ...state,
        blackSection: action.data,
        isReadyBlack: true
      };
    default:
      return state;
  }
};

export const setSettings = (data) => (
  {
    type: SET_SETTINGS,
    data
  });

export const setBlackSection = (data) => (
  {
    type: SET_BLACK_SECTION,
    data
  });

export const getSettings = () => async (dispatch) => {
  let response = await settingsAPI.getSettings();
  dispatch(setSettings(response.data));
};

export const getBlackSection = () => async (dispatch) => {
  let response = await settingsAPI.getBlackSection();
  dispatch(setBlackSection(response.data));
};

export default settingsReducer;