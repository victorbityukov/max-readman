import {articlesAPI} from "../api/api";
import _ from 'lodash';
const SET_ARTICLES = 'SET_ARTICLES';
const SET_CURRENT_ARTICLE = 'SET_CURRENT_ARTICLE';

let initialState = {
  isReadyList: false,
  items: [],
  membership_items: [],
  isReadyCurrent: false,
  current: {},
};

const articlesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ARTICLES:
      return {
        ...state,
        items: action.articles,
        membership_items: _.filter(action.articles, {only_subscribers: 1}),
        isReadyList: true,
      };
    case SET_CURRENT_ARTICLE:
      return {
        ...state,
        isReadyCurrent: true,
        current: action.article,
      };
    default:
      return state;
  }
};

export const setArticles = (articles) => (
  {
    type: SET_ARTICLES,
    articles
  });

export const setCurrentArticle = (article) => (
  {
    type: SET_CURRENT_ARTICLE,
    article
  });

export const getCurrentArticle = (id) => async (dispatch) => {
  let responseArticle = await articlesAPI.getCurrentArticle(id);
  dispatch(setCurrentArticle(responseArticle.data));
};

export const getListArticles = (id) => async (dispatch) => {
  let responseArticles = await articlesAPI.getListArticles(id);
  dispatch(setArticles(responseArticles.data.items));
};

export default articlesReducer;