import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import headerReducer from "./header-reducer";
import menuReducer from "./menu-reducer";
import authReducer from "./auth-reducer";
import settingsReducer from "./settings-reducer";
import articlesReducer from "./articles-reducer";
import categoriesReducer from "./categories-reducer";

let reducers = combineReducers({
  header: headerReducer,
  menu: menuReducer,
  auth: authReducer,
  settings: settingsReducer,
  articles: articlesReducer,
  categories: categoriesReducer
});

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const store = createStore(reducers,  composeEnhancers(applyMiddleware(thunkMiddleware)));
const store = createStore(reducers,  applyMiddleware(thunkMiddleware));
window.__store__ = store;

export default store;