const CHANGE_THEME_HEADER = 'CHANGE_THEME_HEADER';

let initialState = {
  theme: 'light'
};

const headerReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_THEME_HEADER:
      state.theme = action.theme;
      return {
        ...state,
        theme: action.theme
      };
    default:
      return state;
  }
};

export const changeTheme = (theme) =>
  (
    {
      type: CHANGE_THEME_HEADER,
      theme
    }
  );

export default headerReducer;