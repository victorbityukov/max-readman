import {categoriesAPI} from "../api/api";
const SET_CATEGORIES = 'SET_CATEGORIES';
const SET_CURRENT_CATEGORY = 'SET_CURRENT_CATEGORY';

let initialState = {
  isReadyList: false,
  items: [],
  isReadyCurrent: false,
  current: {},
};

const categoriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CATEGORIES:
      return {
        ...state,
        isReadyList: true,
        items: action.data.items,
      };
    case SET_CURRENT_CATEGORY:
      return {
        ...state,
        isReadyCurrent: true,
        current: action.category,
      };
    default:
      return state;
  }
};

export const setCategories = (data) => (
  {
    type: SET_CATEGORIES,
    data
  });

export const setCurrentCategory = (category) => (
  {
    type: SET_CURRENT_CATEGORY,
    category
  });

export const getCurrentCategory = (id) => async (dispatch) => {
  let responseCategory = await categoriesAPI.getCurrentCategory(id);
  dispatch(setCurrentCategory(responseCategory.data));
};

export const getListCategories = (id) => async (dispatch) => {
  let responseCategories = await categoriesAPI.getListCategories(id);
  dispatch(setCategories(responseCategories.data));
};

export default categoriesReducer;