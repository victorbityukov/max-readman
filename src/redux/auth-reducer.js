import {authAPI} from "../api/api";
import {changeMenuByTypeUser} from "./menu-reducer";
const SET_USER_DATA = 'SET_USER_DATA';

let initialState = {
  id: null,
  type: 'anon',
  time_left: null,
  isAuth: false
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      let isAuth = false;
      if (action.data.type !== 'guest') {
        isAuth = true;
      }
      return {
        ...state,
        ...action.data,
        isAuth: isAuth
      };
    default:
      return state;
  }
};

export const setRegister = (email) => async (dispatch) => {
  await authAPI.register(email);
};

export const setLogin = (email, password) => async (dispatch) => {
  let response = await authAPI.login(email, password);
  dispatch(setAuthUserData(response.data));
  dispatch(changeMenuByTypeUser(response.data.type));
};

export const setLogout = () => async (dispatch) => {
  let response = await authAPI.logout();
  dispatch(setAuthUserData(response.data));
  dispatch(changeMenuByTypeUser(response.data.type));
};

export const checkMe = () => async (dispatch) => {
  let response = await authAPI.me();
  dispatch(setAuthUserData(response.data));
  dispatch(changeMenuByTypeUser(response.data.type));
};

export const setAuthUserData = ({id, type, time_left}) => (
  {
    type: SET_USER_DATA,
    data: {id, type, time_left}
  });

export default authReducer;