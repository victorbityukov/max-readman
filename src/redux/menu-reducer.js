const CHANGE_MENU_BY_TYPE_USER = 'CHANGE_MENU_BY_TYPE_USER';

let initialState = {
  items: [
    {
      value: "Статьи",
      link: "/archive",
      items: [
        {
          value: 'Выбор автора',
          link: '/choice-author'
        },
        {
          value: 'Для подписчиков',
          link: '/membership-content'
        }
      ]
    },
    {
      value: "Обо мне",
      link: "/about-me",
      items: []
    },
    {
      value: "Особое",
      link: "/subscription-room",
      items: [
        {
          value: 'Хочу подписку',
          link: '/registration/info'
        },
        {
          value: 'Хочу зайти',
          link: '/login'
        },
      ]
    }
  ]
};

const menuReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_MENU_BY_TYPE_USER:
      switch (action.type_user) {
        case 'admin':
          return {
            items: [
              {
                value: "Статьи",
                link: "/archive",
                items: [
                  {
                    value: 'Выбор автора',
                    link: '/choice-author'
                  },
                  {
                    value: 'Для подписчиков',
                    link: '/membership-content'
                  }
                ]
              },
              {
                value: "Обо мне",
                link: "/about-me",
                items: []
              },
              {
                value: "Особое",
                link: "/subscription-room",
                items: [
                  {
                    value: 'Выход',
                    link: '/exit'
                  }
                ]
              }
            ]
          };
        case 'subscriber':
          return {
            items: [
              {
                value: "Статьи",
                link: "/archive",
                items: [
                  {
                    value: 'Выбор автора',
                    link: '/choice-author'
                  },
                  {
                    value: 'Для подписчиков',
                    link: '/membership-content'
                  }
                ]
              },
              {
                value: "Обо мне",
                link: "/about-me",
                items: []
              },
              {
                value: "Особое",
                link: "/subscription-room",
                items: [
                  {
                    value: 'Выход',
                    link: '/exit'
                  }
                ]
              }
            ]
          };
        default:
          return {
            items: [
              {
                value: "Статьи",
                link: "/archive",
                items: [
                  {
                    value: 'Выбор автора',
                    link: '/choice-author'
                  },
                  {
                    value: 'Для подписчиков',
                    link: '/membership-content'
                  }
                ]
              },
              {
                value: "Обо мне",
                link: "/about-me",
                items: []
              },
              {
                value: "Особое",
                link: "/registration/info",
                items: [
                  {
                    value: 'Хочу подписку',
                    link: '/registration/info'
                  },
                  {
                    value: 'Хочу зайти',
                    link: '/login'
                  },
                ]
              }
            ]
          }
      }
    default:
      return state;
  }
};

export const changeMenuByTypeUser = (type_user) =>
  (
    {
      type: CHANGE_MENU_BY_TYPE_USER,
      type_user
    }
  );

export default menuReducer;