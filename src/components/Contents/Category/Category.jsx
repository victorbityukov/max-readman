import React from 'react'
import Welcome from "../../common/Welcome/Welcome";
import Main from "../../common/Main/Main";
import {connect} from "react-redux";
import Content from "../../common/Content/Content";
import {getCurrentCategory} from "../../../redux/categories-reducer";
import {Redirect} from "react-router-dom";
import RelatedPostsByCategory from "./RelatedPostsByCategory/RelatedPostsByCategory";
import Preloader from "../../common/Preloader/Preloader";


const CategoryContainer = (props) => {
  let id = props.match.params.id;
  let isReady = true;
  if (props.current.id != id) {
    isReady = false;
    props.getCurrentCategory(id);
  }

  return (
    <>
      {(isReady) ? <CategoryContent {...props}/> : <Preloader/>}
    </>
  )
};

let mapStateToProps = ({categories}) => {
  return {
    isReady: categories.isReadyCurrent,
    current: categories.current
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    getCurrentCategory: (id) => {
      dispatch(getCurrentCategory(id));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryContainer);

function CategoryContent({current}) {
  return (
    <Main>
      {!!current.empty ? <Redirect to={"/"}/> : ''}
      <Welcome>
        <h1>{current.title}</h1>
      </Welcome>
      <Content>
        {current.content}
      </Content>
      <RelatedPostsByCategory category={current}/>
    </Main>
  );
}

