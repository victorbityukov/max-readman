import React from 'react';
import style from './RelatedPostsByCategory.module.css';
import {getListArticles} from "../../../../redux/articles-reducer";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {check_categories} from "../../../../utils/functions";

const RelatedPostsByCategory = ({category, isReadyList, items, getListArticles}) => {
  if (!isReadyList) {
    getListArticles();
  }

  let items_current_category = [];
  items.forEach((item) => {
    if (check_categories(item.categories, [category.id])) {
      items_current_category.push(item);
    }
  });
  let related_posts = (items_current_category.length > 0);

  return (
    <div className={style['related-posts']}>
      <h3>Больше статей категории «‎{category.title}»‎</h3>
      <div className={style['posts']}>
        {related_posts ? <>{items_current_category.map((item) => (
          <div key={item.id} className={style['post']}>
            <Link to={`/article/${item.id}`}>{item.title}</Link>
            {item.only_subscribers ? <span className={style["subscribers"]}> (Только для подписчиков)</span> : ''}
          </div>
        ))}</> : 'Статей данной категории не найдено...'}

      </div>
    </div>
  );
};

let mapStateToProps = ({articles}) => {
  return {
    isReadyList: articles.isReadyList,
    items: articles.items
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getListArticles: () => dispatch(getListArticles())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(RelatedPostsByCategory);