import React from 'react';
import AboutMiniContainer from "./AboutMini/AboutMini";
import BlackSection from "./BlackSection/BlackSection";
import ContainerLatestContent from "./ContainerLatestContent/ContainerLatestContent";

export default function Home(props) {
  return (
    <>
      <AboutMiniContainer {...props}/>
      <BlackSection/>
      <ContainerLatestContent/>
    </>
  );
}