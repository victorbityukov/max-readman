import React from 'react';
import {Link} from "react-router-dom";
import style from "./BlackElement.module.css";
import Icon from "@material-ui/core/Icon";

export default function BlackElement({id, title, icon, teaser, index}) {
  return (
    <>
      <Link className={style['link-element']} to={`category/${id}`}>
        <div className={`${style["logo"]} ${style['block' + index]}`}>
          <Icon>{icon}</Icon>
        </div>
        <div className={style['container-data']}>
          <h4>
            {title}
          </h4>
          <div className={style['description-data']}>
            <p>{teaser}</p>
          </div>
          <div className={style['link-data']}>
            <p>Подробнее</p>
          </div>
        </div>
      </Link>
    </>
  );
}