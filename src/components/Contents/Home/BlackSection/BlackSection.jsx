import React from 'react';
import style from "./BlackSection.module.css";
import BlackElement from "./BlackElement/BlackElement";
import {connect} from "react-redux";
import {getBlackSection} from "../../../../redux/settings-reducer";
import {compare_slots} from "../../../../utils/functions";

const BlackSection = ({blackSection, isReady, getBlackSection}) => {
  if (!isReady) {
    getBlackSection();
  }

  if (isReady) {
    blackSection.items.sort(compare_slots);
  }

  return (
    <div className={style["section-black-container"]}>
      <div className={style["section-black"]}>
        <div className="section">
          <div className={style["content-container"]}>
            <div className={style["content"]}>
              <h2>{blackSection.title}</h2>
              <div className={style["grid"]}>
                {isReady ? <>
                    {blackSection.items.map((section, i) => (
                      <div key={section.id}>
                        <BlackElement {...section} index={i}/>
                      </div>
                    ))}
                  </> :
                  ''
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

let mapStateToProps = ({settings}) => {
  return {
    isReady: settings.isReadyBlack,
    blackSection: settings.blackSection
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getBlackSection: () => dispatch(getBlackSection()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(BlackSection)