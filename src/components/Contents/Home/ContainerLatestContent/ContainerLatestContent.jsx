import React from 'react';
import {connect} from "react-redux";
import {getListArticles} from "../../../../redux/articles-reducer";
import Delimiter from "../../../common/Delimiter/Delimiter";
import LatestArticles from "./LatestArticles/LatestArticles";
import MemberShipContent from "./MemberShipContent/MemberShipContent";

const ContainerLatestContent = ({isReady, getListArticles, items, membership_items}) => {
  if (!isReady) {
    getListArticles();
  }
  return (
    <>
      {isReady ? <>
        <LatestArticles items={items}/>
        <Delimiter/>
        <MemberShipContent items={membership_items} count={items.length}/>
      </> : ''}
    </>
  );
};

let mapStateToProps = ({articles}) => {
  return {
    isReady: articles.isReadyList,
    items: articles.items,
    membership_items: articles.membership_items
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getListArticles: () => dispatch(getListArticles())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContainerLatestContent)