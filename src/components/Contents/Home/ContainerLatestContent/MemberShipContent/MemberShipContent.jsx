import React from 'react';
import style from "./MemberShipContent.module.css";
import ButtonLink from "../../../../common/ButtonLink/ButtonLink";
import MembershipArticleTeaser from "./MembershipArticleTeaser/MembershipArticleTeaser";
import {connect} from "react-redux";

const MemberShipContent = ({isAuth, count, items, isReady, config}) => {
  let lastArticles = items.slice(0, 3);
  let textMemberShip = '';
  if (isReady) {
    let tempTextArr = config.membership_text.split('{count}');
    textMemberShip = tempTextArr.join(String(count));
  }
  return (
    <div className="section">
      <h3 className={style["membership-title"]}>Контент для подписчиков</h3>
      <ul className={style['articles']}>
        {lastArticles.map((item) => (
          <li className={style['article']} key={item.id}>
            <MembershipArticleTeaser{...item}/>
          </li>
        ))}
        <li>
          <p>
            {textMemberShip}
          </p>
        </li>
      </ul>
      {!isAuth ?
        <div className={style["all-posts-wrapper"]}>
          <ButtonLink to="/registration/info">Подпишись и получи полный доступ</ButtonLink>
        </div> : ''
      }
    </div>
  );
};

let mapDispatchToProps = ({auth, settings}) => {
  return {
    isAuth: auth.isAuth,
    isReady: settings.isReady,
    config: settings.config
  }
};

export default connect(mapDispatchToProps, {})(MemberShipContent);