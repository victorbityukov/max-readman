import React from 'react';
import style from "./MembershipArticleTeaser.module.css"
import {Link} from "react-router-dom";
import logo512 from '../../../../../../assets/images/logo512.png';

export default function MembershipArticleTeaser(
  {
    id,
    title,
    teaser,
    img_ava
  }) {
  return (
    <>
      <div className={style["image-teaser-container"]}>
        <Link to={`article/${id}`} className={style["item"]}>
          <div className={style["photo"]}>
            <img className={style["personPhoto"]}
                 src={img_ava ? img_ava : logo512} alt={title}/>
          </div>
        </Link>
      </div>
      <div className={style["post-preview"]}>
        <Link to={`article/${id}`}>
          <h2>{title}</h2>
        </Link>
        <p>{teaser}</p>
        <Link className={style['link-more']} to={`article/${id}`}>Читать далее...</Link>
      </div>
    </>
  );
}