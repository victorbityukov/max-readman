import React from 'react';
import style from "./ArticleTeaser.module.css"
import {Link} from "react-router-dom";
import logo512 from '../../../../../../assets/images/logo512.png';

export default function ArticleTeaser(
  {
    id,
    title,
    teaser,
    categories,
    day,
    month,
    year,
    author,
    reading_time,
    img_ava,
    only_subscribers
  }) {
  return (
    <>
      <div className={style["meta"]}>
        <div className={style['container-ava']}>
          <Link to={`article/${id}`} className={style["item"]}>
            <img className={style["personPhoto"]}
                 src={img_ava ? img_ava : logo512} alt={title}/>
          </Link>
        </div>
        <div className={style["data-teaser-container"]}>
          <div>
            <div className={style["meta-title"]}>
              Дата:
            </div>
            <div className={style["meta-value"]}>
              {month} - {day}, {year}
            </div>
          </div>
          <div className={style["meta-category"]}>
            <div className={style["meta-title"]}>Категория:</div>
            <div className={style["meta-value"]}>
              {categories.map((category, i) => (
                <span key={category.id}>
                  {(i ? ', ' : '')}
                  <Link to={`category/${category.id}`}>
                    {category.title}
                  </Link>
                </span>
              ))}
            </div>
          </div>
          <div>
            <div className={style["meta-title"]}>Время чтения:</div>
            <div className={style["meta-value"]}>{reading_time} мин</div>
          </div>
          <div>
            <div className={style["meta-title"]}>Автор:</div>
            <div className={style["meta-value"]}>{author}</div>
          </div>
        </div>
      </div>
      <div className={style["post-preview"]}>
        <Link to={`article/${id}`}>
          <h2 className={style['article-title']}>{title}</h2>
          {!!only_subscribers ? <p className={style["only-subscribers"]}>(Только для подписчиков)</p> : ''}
          <p>{teaser}</p>
        </Link>
      </div>
    </>
  );
}