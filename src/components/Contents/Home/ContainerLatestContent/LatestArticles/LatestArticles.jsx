import React from 'react';
import style from "./LatestArticles.module.css"
import ArticleTeaser from "./ArticleTeaser/ArticleTeaser";
import ButtonLink from "../../../../common/ButtonLink/ButtonLink";

export default function LatestArticles({items}) {
  let lastArticles = items.slice(0, 5);
  return (
    <div className="section">
      <h3 className={style["latest-title"]}>Последние статьи</h3>
      <div className={style['articles']}>
        {lastArticles.map((item) => (
          <div className={style['article']} key={item.id}>
            <ArticleTeaser {...item}/>
          </div>
        ))}
      </div>
      <div className={style["all-posts-wrapper"]}>
        <ButtonLink to="/archive">Кликни для просмотра всех статей</ButtonLink>
      </div>
    </div>
  );
}