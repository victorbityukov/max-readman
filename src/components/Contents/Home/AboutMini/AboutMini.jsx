import React from 'react';
import {Link} from "react-router-dom";
import ButtonLink from "../../../common/ButtonLink/ButtonLink";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFileAlt, faUserAlt} from "@fortawesome/free-solid-svg-icons";
import style from './AboutMini.module.css';
import {connect} from "react-redux";


function AboutMini(props) {
  return (
    <div className={style["section-about"]}>
      <div className={style["container-about"]}>
        <div className="section">
          <div className="content-container">
            <div className="content">
              <h1 className={style['say-hello']}>Привет, я Макс!</h1>
              <p>
                {props.isReadySettings ?
                  props.config.welcome : ''
                }
              </p>
              <h4 className={style["join-line"]}>НАЧАТЬ ЗНАКОМСТВО СО МНОЙ МОЖНО СО СТАТЬИ — <Link to="/about-me">ОБО МНЕ</Link></h4>
              <p className={style["btn-container"]}>
                <ButtonLink to='/archive'><FontAwesomeIcon icon={faFileAlt}/>Читать статьи</ButtonLink>
                {props.isAuth ||
                <ButtonLink to='/registration/info'><FontAwesomeIcon icon={faUserAlt}/>Подписаться</ButtonLink>}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

let mapStateToProps = ({auth, settings}) => {
  return {
    isAuth: auth.isAuth,
    isReadySettings: settings.isReady,
    config: settings.config
  }
};

let AboutMiniContainer = connect(mapStateToProps)(AboutMini);
export default AboutMiniContainer;