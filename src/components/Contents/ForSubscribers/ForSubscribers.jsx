import React from 'react'
import Welcome from "../../common/Welcome/Welcome";
import Main from "../../common/Main/Main";
import {connect} from "react-redux";
import {getListArticles} from "../../../redux/articles-reducer";
import Content from "../../common/Content/Content";
import style from './ForSubscribers.module.css';
import {Link} from "react-router-dom";
import logo512 from '../../../assets/images/logo512.png';
import Preloader from "../../common/Preloader/Preloader";


const ForSubscribersContainer = ({isReadySettings, isReadyList, items, config, getListArticles}) => {
  if (isReadySettings) {
    if (!isReadyList) {
      getListArticles();
    }
  }

  return (
    <>
      {
        (isReadyList && isReadySettings) ?
          <ForSubscribersContent
            articles={items}
            page_for_subscribers={JSON.parse(config.page_for_subscribers)}/> : <Preloader/>
      }
    </>
  )
};

let mapStateToProps = ({settings, articles}) => {
  return {
    isReadySettings: settings.isReady,
    config: settings.config,
    isReadyList: articles.isReadyList,
    items: articles.membership_items
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    getListArticles: () => {
      dispatch(getListArticles());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ForSubscribersContainer);

function ForSubscribersContent({articles, page_for_subscribers}) {
  return (
    <Main>
      <Welcome>
        <h1>{page_for_subscribers.title}</h1>
        <h4 className={style['subtitle']}>{page_for_subscribers.subtitle}</h4>
      </Welcome>
      <Content>
        {page_for_subscribers.content}
      </Content>
      <div className={style['articles']}>
        {articles ? articles.map((article, i) => (
          <div className={style["container-article"]} key={i}>
            <Link className={style['article']} to={`/article/${article.id}`}>
              <img className={style['ava']} src={article.img_ava ? article.img_ava : logo512} alt={article.title}/>
              <div className={style["shadow"]}/>
              <div className={style["title"]}>
                {article.title}
              </div>
              <div className={style["teaser"]}>
                {article.teaser}
              </div>
            </Link>
          </div>
        )) : ''}
      </div>
    </Main>
  );
}

