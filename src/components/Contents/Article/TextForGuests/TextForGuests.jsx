import React from 'react';
import style from './TextForGuests.module.css';
import {Link} from "react-router-dom";

const TextForGuests = () => {
  return (
    <div className={style['container-guests']}>
      <div className={style['gradient']}/>
      <h3>Прости, что так грубо прерываю</h3>
      <p>Это статья для подписчиков сайта Readman. Если ты ещё не в банде — кликни вот <Link to={"/registration/info"}>ТУТ</Link> и-и-и ты сможешь стать одним из нас.</p>
      <p>Если ты уже авторизован, но всё ещё видишь это сообщение — значит действие подписки закончилось. Продлить подписку ты можешь вот <Link to={"/subscription-room"}>КЛАЦ</Link>.</p>
    </div>
  );
};

export default TextForGuests;