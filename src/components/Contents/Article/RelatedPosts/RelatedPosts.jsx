import React from 'react';
import style from './RelatedPosts.module.css';
import {getListArticles} from "../../../../redux/articles-reducer";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {check_categories} from "../../../../utils/functions";
import logo512 from '../../../../assets/images/logo512.png';

const RelatedPosts = ({id_article, categories, isReadyList, items, getListArticles}) => {
  if (!isReadyList) {
    getListArticles();
  }

  let items_current_category = [];
  items.forEach((item) => {
    if (id_article != item.id) {
      if (check_categories(item.categories, categories)) {
        items_current_category.push(item);
      }
    }
  });

  return (
    <div className={style['related-posts']}>
      <h3>Похожие статьи</h3>
      <div className={style['posts']}>
        {items_current_category.map((item) => (
          <div key={item.id} className={style['post']}>
            <Link to={`/article/${item.id}`}>
              <img src={item.img_ava ? item.img_ava : logo512} alt={item.title}/>
              <p className={style['title']}>{item.title}</p>
              <span className={style["subscribers"]}> {item.only_subscribers ? <>(Только для
                подписчиков)</> : <>&ensp;</>} </span>
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
};

let mapStateToProps = ({articles}) => {
  return {
    isReadyList: articles.isReadyList,
    items: articles.items
  }
};

let mapDispatchToProps = dispatch => {
  return {
    getListArticles: () => dispatch(getListArticles())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(RelatedPosts);