import React from 'react';
import style from './Article.module.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronDown} from "@fortawesome/free-solid-svg-icons";
import RelatedPosts from "./RelatedPosts/RelatedPosts";
import {connect} from "react-redux";
import {compose} from "redux";
import {darkTheme} from "../../../hoc/darkTheme";
import {getCurrentArticle} from "../../../redux/articles-reducer";
import Content from "../../common/Content/Content";
import {Redirect} from "react-router-dom";
import TextForGuests from "./TextForGuests/TextForGuests";


const ArticleContainer = (props) => {
  let id = props.match.params.id;
  let isReady = true;
  if (props.current.id != id) {
    isReady = false;
    props.getCurrentArticle(id);
  }
  return (
    <>
      {(isReady) ? <ArticleContent {...props}/> : ''}
    </>
  )
};

let mapStateToProps = ({auth, articles}) => {
  return {
    typeUser: auth.type,
    timeLeft: auth.time_left,
    isReadyArticle: articles.isReadyCurrent,
    current: articles.current
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    getCurrentArticle: (id) => {
      dispatch(getCurrentArticle(id));
    }
  }
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  darkTheme
)(ArticleContainer);

function ArticleContent({typeUser, timeLeft, current}) {
  return (
    <div className={style["main"]}>
      {!!current['empty'] ? <Redirect to={"/"}/> : ''}
      <div className={style["welcome"]} style={{"backgroundImage":`url(${current.img_back})`}}>
        <div className={style["overlay"]}>
        </div>
        <div className={style["container-title"]}>
          <h1>{current.title}</h1>
          <p>{current.subtitle}</p>
          <p className={style["meta"]}>
            <span className={style["reading-time"]}>{current.reading_time} мин</span> — <span>{current.author}</span>
          </p>
        </div>
        <span className={style["article-arrow"]}>
            <a href={"#article-content"}><FontAwesomeIcon icon={faChevronDown}/></a>
          </span>
      </div>
      <div id={"article-content"} className={"entry-content"}>
        <Content>
          {current.content}
        </Content>
        {((current.only_subscribers === 0) || ((typeUser !== 'guest') && (timeLeft > 0))) ? <>
          <hr/>
          <RelatedPosts id_article={current.id} categories={current.id_categories}/>
        </> : <TextForGuests/>
        }
      </div>
    </div>
  );
}

