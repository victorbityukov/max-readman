import React from 'react';
import style from './RegistrationInfo.module.css';
import Main from "../../../common/Main/Main";
import Welcome from "../../../common/Welcome/Welcome";
import ButtonLink from "../../../common/ButtonLink/ButtonLink";
import {connect} from "react-redux";

const RegistrationInfo = ({isReadySettings, config}) => {
  let register_info = {title: '', subtitle: '', content: ''};
  if (isReadySettings) {
    register_info = JSON.parse(config.register_info);
  }

  return (
    <Main>
      <Welcome>
        <h1>{register_info.title}</h1>
        <h2>{register_info.subtitle}</h2>
        <p className={style['content']}>{register_info.content}</p>
      </Welcome>
      <div className={style['btn-container']}>
        <ButtonLink to="/registration-pay">Подписаться</ButtonLink>
      </div>
    </Main>
  );
};


let mapStateToProps = ({auth, settings}) => {
  return {
    isReadySettings: settings.isReady,
    config: settings.config
  }
};

let AboutMiniContainer = connect(mapStateToProps)(RegistrationInfo);
export default AboutMiniContainer;