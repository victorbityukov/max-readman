import React from 'react';
import AuthorizationForm from "./AuthorizationForm/AuthorizationForm";
import Main from "../../../common/Main/Main";
import Welcome from "../../../common/Welcome/Welcome";
import {compose} from "redux";
import {withMainRedirectForSubscribe} from "../../../../hoc/withMainRedirectForSubscribe";
import ButtonLink from "../../../common/ButtonLink/ButtonLink";
import style from './Authorization.module.css'

function Authorization() {
  return (
    <Main>
      <Welcome>
        <h1 className={style['title']}>Свой? Заходи</h1>
        <AuthorizationForm/>
      </Welcome>
      <div className={style['container-register']}>
        <ButtonLink to={"/registration/info"}>Вступить в банду</ButtonLink>
      </div>
    </Main>
  );
}

export default compose(
  withMainRedirectForSubscribe)
(Authorization);