import React from 'react';
import style from './AuthorizationForm.module.css';
import {Formik} from "formik";
import Button from "../../../../common/Button/Button";
import {setLogin} from "../../../../../redux/auth-reducer";
import {connect} from "react-redux";


const AuthorizationForm = ({setLogin}) => {
  return (
    <Formik
      initialValues={
        {
          email: '',
          password: ''
        }
      }
      validate={values => {
        const errors = {};
        if (values.email &&
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Почта некорректна';
        }
        if (!values.email) {
          errors.email = 'Email должен быть заполнен';
        }
        return errors;
      }}
      onSubmit={(values, {setSubmitting}) => {
        let {email, password} = values;
        setLogin(email, password);
        setSubmitting(false);
      }}
    >
      {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
        <form className={style['form-container']} onSubmit={handleSubmit}>
          <input
            className={style["element-form"]}
            type="email"
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
            placeholder={"Email"}
          />
          <p className={style['error']}>{errors.email && touched.email && errors.email}</p>
          <input
            className={style["element-form"]}
            type="password"
            name="password"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.password}
            placeholder={"Пароль"}
          />
          <div className={style['container-submit']}>
            <Button>Уже вхожу</Button>
          </div>
        </form>
      )}
    </Formik>
  )
};

let mapStateToProps = () => {
  return {}
};

let mapDispatchToProps = (dispatch) => {
  return {
    setLogin: (email, password) => {
      dispatch(setLogin(email, password));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationForm);