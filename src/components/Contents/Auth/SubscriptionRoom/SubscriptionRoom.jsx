import React from 'react';
import SubscriptionRoomForm from "./SubscriptionRoomForm/SubscriptionRoomForm";
import ButtonLink from "../../../common/ButtonLink/ButtonLink";
import style from './SubscriptionRoom.module.css';
import {connect} from "react-redux";
import Preloader from "../../../common/Preloader/Preloader";
import {Redirect} from "react-router-dom";
import {is_auth} from "../../../../utils/functions";

const SubscriptionRoom = ({auth}) => {
  let subscribe_info = '';
  if (auth.isAuth) {
    if (is_auth(auth.type)) {
      if (auth.time_left > 0) {
        let days_left = Math.round(auth.time_left / 86400);
        subscribe_info = `${days_left}`;
      } else {
        subscribe_info = 'Срок подписки окончен';
      }
    }
  }
  return (
    <div className={style['main']}>
      {auth.type !== 'anon' ?
        <div className={style['welcome']}>
          {auth.type === 'guest' ? <Redirect to={"/login"}/> : ''}
          <div className={style['main-container']}>
            <div className={style['container-info']}>
              <h1>Личный кабинет</h1>
              <p>Дней подписки осталось:</p>
              <h3>{subscribe_info}</h3>
            </div>
            <SubscriptionRoomForm/>
            <div className={style['container-login']}>
              <ButtonLink to={"/"}>На главную</ButtonLink>
            </div>
          </div>
        </div> :
        <Preloader/>
      }
    </div>
  );
};

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};
export default connect(mapStateToProps, {})(SubscriptionRoom);