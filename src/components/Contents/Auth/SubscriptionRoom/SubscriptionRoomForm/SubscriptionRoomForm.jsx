import React, {Component} from 'react';
import style from './SubscriptionRoomForm.module.css';

const SubscriptionRoomForm = () => {
  return (
    <div className={style['container-form']}>
      <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
        <input type="hidden" name="receiver" value="4100115825563277"/>
        <input type="hidden" name="formcomment" value="Продление подписки на блог Max Readman"/>
        <input type="hidden" name="short-dest" value="Продление подписки на Max Readman"/>
        <input type="hidden" name="quickpay-form" value="donate"/>
        <input type="hidden" name="targets" value="Продление подписки"/>
        <div className={style['container-columns']}>
          <div style={{marginBottom: '10px'}}>
            {/*<label><input type="radio" name="sum" value="10" data-type="number"/>Тест 10р.</label>*/}
            {/*<br/>*/}
            <label><input type="radio" name="sum" value="399" data-type="number"/>Продлить на месяц (399р.)</label>
            <br/>
            <label><input type="radio" name="sum" value="2499" data-type="number"/>Продлить на год (2499р.)</label>
          </div>
          <div>
            <label><input type="radio" name="paymentType" value="PC"/>Яндекс Деньги</label>
            <br/>
            <label><input type="radio" name="paymentType" value="AC"/>Банковская карта</label>
          </div>
        </div>
        <input type="hidden" name="need-email" value="true"/>
        <div className={style['container-submit']}>
          <input className={style['submit']} type="submit" value="Перевести"/>
        </div>
      </form>
    </div>
  );
};

export default SubscriptionRoomForm;