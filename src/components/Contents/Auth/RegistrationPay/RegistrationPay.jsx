import React from 'react';
import ButtonLink from "../../../common/ButtonLink/ButtonLink";
import style from './RegistrationPay.module.css';
import {connect} from "react-redux";
import Preloader from "../../../common/Preloader/Preloader";
import {Redirect} from "react-router-dom";
import RegistrationPayForm from "./RegistrationPayForm/RegistrationPayForm";

const SubscriptionRoom = ({auth}) => {
  return (
    <div className={style['main']}>
      {auth.type !== 'anon' ?
        <div className={style['welcome']}>
          {auth.type !== 'guest' ? <Redirect to={"/subscription-room"}/> : ''}
          <div className={style['main-container']}>
            <div className={style['container-info']}>
              <h1>Оформляем</h1>
            </div>
            <RegistrationPayForm/>
            <div className={style['container-login']}>
              <ButtonLink to={"/login"}>Авторизоваться</ButtonLink>
            </div>
          </div>
        </div> :
        <Preloader/>
      }
    </div>
  );
};

let mapStateToProps = ({auth}) => {
  return {
    auth
  }
};
export default connect(mapStateToProps, {})(SubscriptionRoom);