import React from 'react';
import {connect} from "react-redux";
import style from './Logout.module.css';
import Main from "../../../common/Main/Main";
import Welcome from "../../../common/Welcome/Welcome";
import Button from "../../../common/Button/Button";
import {setLogout} from "../../../../redux/auth-reducer";
import {compose} from "redux";
import {withMainRedirectForGuest} from "../../../../hoc/withMainRedirectForGuest";

function Logout({type, setLogout}) {
  const onExit = () => {
    setLogout();
  };
  return (
    <Main>
      <Welcome>
        <h1>Ты правда хочешь уйти?</h1>
        <h2>Одевайся теплее. Зима близко.</h2>
        <p className={style['btn-container']}>
          <Button onClick={onExit}>Закрыть за собой дверь</Button>
        </p>
      </Welcome>
    </Main>
  );
}

let mapStateToProps = ({auth}) => {
  return {
    type: auth.type,
    isAuth: auth.isAuth
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    setLogout: () => {
      dispatch(setLogout());
    }
  }
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withMainRedirectForGuest)
(Logout);