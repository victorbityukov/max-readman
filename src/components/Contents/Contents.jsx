import React from 'react'
import {Route, Switch} from "react-router-dom";
import Archive from "./Archive/Archive";
import Home from "./Home/Home";
import About from "./About/About";
import RegistrationInfo from "./Auth/RegistrationInfo/RegistrationInfo";
import Authorization from "./Auth/Authorization/Authorization";
import Logout from "./Auth/Logout/Logout";
import Article from "./Article/Article";
import ScrollToTop from "../common/ScrollToTop/ScrollToTop";
import Category from "./Category/Category";
import ChoiceAuthor from "./ChoiceAuthor/ChoiceAuthor";
import ForSubscribers from "./ForSubscribers/ForSubscribers";
import style from './Contents.module.css'
import RegistrationPay from "./Auth/RegistrationPay/RegistrationPay";
import SubscriptionRoom from "./Auth/SubscriptionRoom/SubscriptionRoom";

const Contents = () => {
  return (
    <div className={style['content']}>
      <ScrollToTop/>
      <Switch>
        <Route path="/archive" component={Archive}/>
        <Route path="/about-me" component={About}/>
        <Route path="/article/:id" component={Article}/>
        <Route path="/category/:id" component={Category}/>
        <Route path="/login" component={Authorization}/>
        <Route path="/subscription-room" component={SubscriptionRoom}/>
        <Route path="/registration/info" component={RegistrationInfo}/>
        <Route path="/registration-pay" component={RegistrationPay}/>
        <Route path="/exit" component={Logout}/>
        <Route path="/choice-author" component={ChoiceAuthor}/>
        <Route path="/membership-content" component={ForSubscribers}/>
        <Route path="/" component={Home}/>
      </Switch>
    </div>
  );
};

export default Contents;