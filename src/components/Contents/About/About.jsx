import React from 'react'
import Welcome from "../../common/Welcome/Welcome";
import Main from "../../common/Main/Main";
import {connect} from "react-redux";
import {getCurrentArticle} from "../../../redux/articles-reducer";
import Content from "../../common/Content/Content";
import Preloader from "../../common/Preloader/Preloader";


const AboutContainer = (props) => {
  if (props.isReadySettings) {
    if (props.current.id != props.config.about_me) {
      props.getCurrentArticle(props.config.about_me);
    }
  }

  return (
    <>
      {props.isReadySettings ?
        props.isReadyArticle ?
          (props.config.about_me == props.current.id) ? <AboutContent {...props}/> : <Preloader/>
          :
          <Preloader/> :
        <Preloader/>}
    </>
  )
};

let mapStateToProps = ({settings, articles}) => {
  return {
    isReadySettings: settings.isReady,
    config: settings.config,
    isReadyArticle: articles.isReadyCurrent,
    current: articles.current
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    getCurrentArticle: (id) => {
      dispatch(getCurrentArticle(id));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AboutContainer);

function AboutContent({current}) {
  return (
    <Main>
      <Welcome><h1>{current.title}</h1></Welcome>
      <Content>
        {current.content}
      </Content>
    </Main>
  );
}

