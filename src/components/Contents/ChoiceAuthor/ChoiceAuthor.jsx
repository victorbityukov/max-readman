import React from 'react'
import Welcome from "../../common/Welcome/Welcome";
import Main from "../../common/Main/Main";
import {connect} from "react-redux";
import {getListArticles} from "../../../redux/articles-reducer";
import style from './ChoiceAuthor.module.css';
import {Link} from "react-router-dom";
import Preloader from "../../common/Preloader/Preloader";


const ChoiceAuthorContainer = ({isReadySettings, isReadyList, items, config, getListArticles}) => {
  if (isReadySettings) {
    if (!isReadyList) {
      getListArticles();
    }
  }
  return (
    <>
      {
        (isReadyList && isReadySettings) ?
          <ChoiceAuthorContent
            articles={items}
            choice_author={JSON.parse(config.choice_author)}/> : <Preloader/>
      }
    </>
  )
};

let mapStateToProps = ({settings, articles}) => {
  return {
    isReadySettings: settings.isReady,
    config: settings.config,
    isReadyList: articles.isReadyList,
    items: articles.items
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    getListArticles: () => {
      dispatch(getListArticles());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChoiceAuthorContainer);

function ChoiceAuthorContent({articles, choice_author}) {
  choice_author.blocks.forEach((block, i) => {
    choice_author.blocks[i].articles_full = [];
    articles.forEach((article) => {
      if (block.articles.indexOf(article.id) != -1) {
        choice_author.blocks[i].articles_full.push(article);
      }
    })
  });

  return (
    <Main>
      <Welcome>
        <h1>{choice_author.title}</h1>
        <h4 className={style['subtitle']}>{choice_author.subtitle}</h4>
      </Welcome>
      <div className={style["blocks"]}>
        {choice_author.blocks.map((block, i) => (
          <div key={i} className={style['block']}>
            <p className={style['info']}>
              {block.text}
            </p>
            <div className={style['articles']}>
              {block.articles_full.map((article) => (
                <div className={style['article']} key={article.id}>
                  <Link to={`/article/${article.id}`}>{article.title}</Link>
                </div>
              ))}
            </div>
          </div>))
        }
      </div>
    </Main>
  );
}


