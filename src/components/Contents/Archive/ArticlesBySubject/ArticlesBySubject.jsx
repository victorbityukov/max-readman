import React from 'react';
import style from './ArticlesBySubject.module.css';
import {Link} from "react-router-dom";
import {getListCategories} from "../../../../redux/categories-reducer";
import {connect} from "react-redux";
import _ from 'lodash';

const ArticlesBySubject = ({isReady, categories, getListCategories}) => {
  if (!isReady) {
    getListCategories();
  }
  let idsGroups = [];
  categories.forEach((category) => {
    if (idsGroups.indexOf(category['id_group']) == -1) {
      idsGroups.push(
        {
          'id': category['id_group'],
          'title': category['title_group']
        });
    }
  });
  idsGroups = _.uniqBy(idsGroups, 'id');
  return (
    <div className={style['by-subject']}>
      <h2>Статьи по категориям</h2>
      <div className={style["archive-grid"]}>
        {idsGroups.map((group, i) => (
          <div className={style["block-category"]} key={i}>
            <strong>{group.title}</strong>
            {categories.map((category) => (
              <Link to={`category/${category.id}`} key={category.id}>
                {
                  category['id_group'] === group.id ?
                    <>{category.title}</>
                    : ''
                }
              </Link>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};

let mapStateToProps = ({categories}) => {
  return {
    isReady: categories.isReadyList,
    categories: categories.items
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    getListCategories: () => dispatch(getListCategories())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesBySubject);