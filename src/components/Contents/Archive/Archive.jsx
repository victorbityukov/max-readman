import React from 'react';
import style from './Archive.module.css';
import ArticlesBySubject from "./ArticlesBySubject/ArticlesBySubject";
import Welcome from "../../common/Welcome/Welcome";
import ArticlesByDate from "./ArticlesByDate/ArticlesByDate";

export default function Archive() {
  return (
    <div className={style['main']}>
      <Welcome>
        <h1>Архив</h1>
        <p>Добро пожаловать в библиотеку джедаев. Здесь ты найдешь тысячелетнюю мудрость человечества распределенную по
          понятным категориям. Или можешь познакомиться со статьями в хронологическом порядке. Наслаждайся.
        </p>
      </Welcome>
      <ArticlesBySubject/>
      <ArticlesByDate/>
    </div>
  );
}