import React from 'react';
import style from './ArticlesByDate.module.css';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import _ from 'lodash';
import {getListArticles} from "../../../../redux/articles-reducer";
import {min_str} from "../../../../utils/functions";

const ArticlesByDate = ({isReady, articles, getListArticles}) => {
  if (!isReady) {
    getListArticles();
  }
  let years = [];
  let articlesByYear = {};
  articles.forEach((article) => {
    if (years.indexOf(article['year']) == -1) {
      years.push(article['year']);
      articlesByYear[article['year']] = [];
    }
    articlesByYear[article['year']].push(article);
  });


  years = _.uniqBy(years);
  return (
    <div className={style['by-date']}>
      <h2>Статьи по дате</h2>
      {years.map((year) => (
        <div className={style['by-year']} key={year}>
          <div className={style["archive-year"]}>{year}</div>
          <table className={style["articles-list"]}>
            <tbody>
            {articlesByYear[year].map((article) => (
              <tr className={style["article-line"]} key={article['id']}>
                <td className={style["date"]}><span
                  className={style['date-desktop']}>{`${article['month']} ${article['day']}`}</span><span
                  className={style['date-mobile']}>{`${min_str(article['month'], 3)} ${article['day']}`}</span></td>
                <td className={style['title-article']}>
                  <Link to={`/article/${article['id']}`}>{article['title']}</Link>
                  {!!article['only_subscribers'] ?
                    <span className={style["subscribers-only"]}> (Только для подписчиков)</span> : ""}
                </td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
      ))}
    </div>
  );
};

let mapStateToProps = ({articles}) => {
  return {
    isReady: articles.isReadyList,
    articles: articles.items
  }
};

let mapDispatchToProps = (dispatch) => {
  return {
    getListArticles: () => dispatch(getListArticles())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesByDate);