import React from 'react';
import {Link} from "react-router-dom";
import style from "./Header.module.css";
import Menu from "./Menu/Menu";
import {connect} from "react-redux";

const Header = ({theme, isReady, config}) => {
  return (
    <header className={style[theme]}>
      <nav className={style["title-container"]}>
        <h1 className={style['title']}><Link to="/">Max Readman</Link></h1>
        <p className={style['subtitle']}>{isReady ? config.subtitle : ''}</p>
      </nav>
      <Menu/>
    </header>
  );
};

let mapStateToProps = ({header, settings}) => {
  return {
    theme: header.theme,
    isReady: settings.isReady,
    config: settings.config
  }
};

let mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);