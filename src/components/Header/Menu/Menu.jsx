import React, {useState} from "react";
import style from "./Menu.module.css";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faWindowClose} from "@fortawesome/free-solid-svg-icons";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import {connect} from "react-redux";

const Menu = ({items}) => {
  const [statusMenu, toggleMenu] = useState('close');
  let openMenu = () => {
    toggleMenu('active');
  };
  let closeMenu = () => {
    toggleMenu('close');
  };
  return (
    <nav className={`${style["menu-container"]} ${style[statusMenu]}`}>
      <div className={style["mobile-bar-close"]} onClick={closeMenu}>
        <FontAwesomeIcon icon={faWindowClose}/>
      </div>
      <ul className={style["primary-menu"]}>
        {items.map((item, i) => (
          <li key={i} className={style["menu-item"]}>
            <Link to={item['link']} onClick={closeMenu}>{item['value']}</Link>
            <ul className={style["sub-menu"]}>
              {item['items'].map((submenu, i) => (
                <li key={i} className={style["menu-item"]}>
                  <Link to={submenu['link']} onClick={closeMenu}>{submenu['value']}</Link>
                </li>))}
            </ul>
          </li>
        ))}
      </ul>
      <div className={style["mobile-bar"]} onClick={openMenu}>
        <FontAwesomeIcon icon={faBars}/>
      </div>
    </nav>
  )
};


let mapStateToProps = ({menu}) => {
  return {
    items: menu.items,
  }
};

let mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
