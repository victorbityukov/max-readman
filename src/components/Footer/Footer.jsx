import React from 'react';
import style from './Footer.module.css';
import ButtonRed from "../common/ButtonRed/ButtonRed";
import {connect} from "react-redux";
import ContentFooter from "../common/ContentFooter/ContentFooter";

const Footer = ({isAuth, isReady, timeLeft, config}) => {
  let footer = {
    title: '',
    content: ''
  };

  if (isReady) {
    footer = JSON.parse(config.footer)
  }

  let subscribe_info = '';
  if (isAuth) {
    if (timeLeft > 0) {
      let days_left = Math.round(timeLeft / 86400);
      subscribe_info = `Осталось дней: ${days_left}`;
    } else {
      subscribe_info = 'Срок подписки окончен';
    }
  }

  return (
    <footer className={style['footer']}>
      <div className={style['footer-container']}>
        <div className={style["content-container"]}>
          {isReady ? <>
            <h1 className={style['title']}>
              {isAuth ? footer.subscriber.title : footer.guest.title}
            </h1>
            <ContentFooter>
              {isAuth ? footer.subscriber.content : footer.guest.content}
            </ContentFooter>
          </> : ''}
        </div>
        {!isAuth ?
          <p className={style["click-center"]}>
            <ButtonRed to="/registration/info">Нажми на меня</ButtonRed>
          </p> :
          <p className={`${style["click-center"]} ${style["grid"]}`}>
            <span>{subscribe_info}</span>
            <ButtonRed to="/subscription-room">Продлить</ButtonRed>
          </p>
        }
        <p className={style['copyright']}>© 2021 Max Readman</p>
      </div>
    </footer>
  );
};

let mapDispatchToProps = ({auth, settings}) => {
  return {
    timeLeft: auth.time_left,
    isAuth: auth.isAuth,
    isReady: settings.isReady,
    config: settings.config
  }
};

export default connect(mapDispatchToProps, {})(Footer);