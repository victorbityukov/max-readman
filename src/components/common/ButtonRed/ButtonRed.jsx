import React from 'react';
import {Link} from "react-router-dom";
import style from './ButtonRed.module.css';


export default function ButtonRed(props) {
  return (
    <Link className={style['btn']} {...props}>{props.children}</Link>
  );
}