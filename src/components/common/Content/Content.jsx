import React from 'react';
import style from './Content.module.css';

export default function Content(props) {
  return (
    <div className={`${style['content']} ck-content`} dangerouslySetInnerHTML={{
      __html:
      props.children
    }}/>
  );
}