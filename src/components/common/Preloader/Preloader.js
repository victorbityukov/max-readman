import React from 'react';
import css from "./Preloader.module.css";

const Preloader = () => {
  return <div className={css['preloader-container']}></div>;
};


export default Preloader;