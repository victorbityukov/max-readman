import React from 'react';
import style from './ContentFooter.module.css';

export default function ContentFooter(props) {
  return (
    <div className={`${style['content']} ck-content`} dangerouslySetInnerHTML={{
      __html:
      props.children
    }}/>
  );
}