import React from 'react';
import style from './Welcome.module.css';

export default function Welcome(props) {
  return (
    <div className={style['welcome']} {...props}>
      {props.children}
    </div>
  );
}