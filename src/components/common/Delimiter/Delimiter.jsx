import React from 'react';
import style from './Delimiter.module.css';

export default function Delimiter() {
  return (
  <div className={style["delimiter"]}>
  </div>
  );
}