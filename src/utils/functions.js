export const min_str = (str, length) => {
  return str.slice(0, length);
};

export const check_categories = (categories, find_categories) => {
  let result = false;
  if ((categories.length) > 0 && (find_categories.length > 0)) {
    categories.forEach(category => {
      find_categories.forEach(category_find => {
        if (category.id === category_find) {
          result = true;
        }
      })
    });
  }
  return result;
};

export const compare_slots = (a, b) => {
  if (a.slot < b.slot)
    return -1;
  if (a.slot > b.slot)
    return 1;
  return 0;
};

export const is_auth = (type) => {
  let types_anon = ['guest', 'anon'];
  return !types_anon.includes(type)
};