import React from "react";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";

let mapStateToPropsForRedirect = ({auth}) => ({
  isAuth: auth.isAuth
});

export const withMainRedirectForGuest = (Component) => {
  class RedirectComponent extends React.Component {
    render() {
      if (!this.props.isAuth) return <Redirect to='/'/>;
      return <Component {...this.props}/>
    }
  }
  let ConnectedMainRedirectComponent = connect(mapStateToPropsForRedirect)(RedirectComponent);
  return ConnectedMainRedirectComponent;
};