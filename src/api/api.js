import * as axios from "axios";

const instance = axios.create({
  baseURL: 'http://localhost/maxblog/api/',
  // baseURL: 'https://mm.maxreadman.ru/api/',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  withCredentials: true
});

export const authAPI = {
  me() {
    let formData = new FormData();
    formData.append('event', 'check');
    return instance.post(`auth`, formData).then((response) => {
      return response.data;
    });
  },
  register(email) {
    let formData = new FormData();
    formData.append('event', 'register');
    formData.append('email', email);
    return instance.post(`auth`, formData);
  },
  login(email, password) {
    let formData = new FormData();
    formData.append('event', 'login');
    formData.append('email', email);
    formData.append('password', password);
    return instance.post(`auth`, formData).then((response) => {
      return response.data;
    });
  },
  logout() {
    let formData = new FormData();
    formData.append('event', 'logout');
    return instance.post(`auth`, formData).then((response) => {
      return response.data;
    });
  }
};

export const settingsAPI = {
  getSettings() {
    let formData = new FormData();
    formData.append('event', 'getSettings');
    return instance.post(`settings`, formData).then((response) => {
      return response.data;
    });
  },
  getBlackSection() {
    let formData = new FormData();
    formData.append('event', 'getBlackSections');
    return instance.post(`settings`, formData).then((response) => {
      return response.data;
    });
  },
};


//статьи
export const articlesAPI = {
  getListArticles() {
    let formData = new FormData();
    formData.append('event', 'getListArticles');
    return instance.post(`articles`, formData).then((response) => {
      return response.data;
    });
  },
  getCurrentArticle(id) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('event', 'getCurrentArticle');
    return instance.post(`articles`, formData).then((response) => {
      return response.data;
    });
  },
};

//категории
export const categoriesAPI = {
  getListCategories() {
    let formData = new FormData();
    formData.append('event', 'getListCategoriesWithGroups');
    return instance.post(`categories`, formData).then((response) => {
      return response.data;
    });
  },
  getCurrentCategory(id) {
    let formData = new FormData();
    formData.append('id', id);
    formData.append('event', 'getCurrentCategory');
    return instance.post(`categories`, formData).then((response) => {
      return response.data;
    });
  },
};


